```
@startuml
start

if (Action) then (BUY)
  :Vantra token buy (with BNB);
  :Supply increased proportionally;
  fork
    :Vantra sent to user;
  fork again
    :Reinvest //(small %)//;
    split
      if (Applied referral link) then (yes)
        if(Less than seven days ago?) then (yes)
          :Apply discount;
          :Dividens wallet (50%);
        else (no)
          :Dividens wallet;
        endif
      else (no)
        :Dividens wallet;
      endif
    split again
      :Team maintenance;
    split again
      :Burn //or referral//;
    end split
  end fork
  stop
else ( )
  if (Other action) then (Lottery) 
     :Buy lottery ticket (VANTRA);
     :Lottery results;
     if (Any winner?) then (yes)
         :Distribute
         | Winners | Burn | Team |
         | 75% | 20% | 5% |;
     else (no)
         :Accumulate for Jackpot
         | Jackpot| Burn | Team |
         | 75% | 20% | 5% |;
     endif
     stop
  else (Sell VANTRA)
    :Vantra token sell (receive BNB);
    stop
  endif
endif
@enduml
```
