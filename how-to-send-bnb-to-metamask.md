# How to send BNB to metamask

1. Open your wallet in Binance exchange, select Binance coin (BNB), and click withdraw.

![Open binance exchange](https://crypto-explained.com/wp-content/uploads/2021/02/open-binace-exchange.png)

2. Go and open MetaMask. Make sure that the wallet is on the Binance Smart Chain network and after that copy the address by clicking on it.

![Copy your metamask wallet​​](https://crypto-explained.com/wp-content/uploads/2021/02/copy-your-meteamask-wallet.png)

3. Paste the address you copied from MetaMask in the address field. Click Binance Smart Chain and enter the amount you wish to transfer below and submit. You should see the BNB coin on the left side.

![Enter details​​](https://crypto-explained.com/wp-content/uploads/2021/02/3.-enter-details.png)

4. Binance will ask for confirmation. Here is the email and phone email but could be a bit different on your screen.

![Confirm](https://crypto-explained.com/wp-content/uploads/2021/02/4.-confirm.png)

5. On the next window you will get a confirmation for withdrawal.

![Confirmation](https://crypto-explained.com/wp-content/uploads/2021/02/5.-confirmation-.png)

After a minute the transaction will appear in your MetaMask wallet so you will be good to go. I hope the guide was helpful and you have learned how to send BNB Coin to Binance Smart Chain on MetaMask.
​

Source: https://crypto-explained.com/services/send-bnb-coin-to-binance-smart-chain-on-metamask/
