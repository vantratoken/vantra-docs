<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Vantra Finance

### Vantra Finance - An innovative yield ecosystem running on the BSC.

Vantra Finance a decentralized exchange running on Binance Smart Chain, with lots of features that let you earn and win tokens.

### What can Vantra do for you?
* Hold and earn. 
  1. Vantra token is SAFE. The secret is the token price: is attached to BNB so your holdings are safe.
  2. Earn dividends by holding VANTRA. With every swap we reinvest a small percent on the highest performing platforms of DeFi, Yield Farms, Staking, Lending, Trading, Stock Market and Investments, in order to pay dividends to all holders by increasing the contract balance.

* Referrals. Earn VANTRA on each deposit made by your referrals. Your referrals will earn VANTRA too on each deposit during the first week.

* Invest in people you trust. We allow you to stake money on popular investors and get great returns.

* And more... We are working on brand new projects, never seen before on Binance Smart Chain. 

### VANTRA is different to any token than any you know...

The secret is that VANTRA TOKEN is an auto-supplied, BNB-price-linked token. Every time you swap BNB in order to get VANTRA TOKEN, it increases its supply proportionally. When you swap VANTRA in order to get BNB, you receive the BNB on your wallet, and the VANTRA tokens are burned.


We innovate the yield farm, you stake or farm a deflationary token and you get profits without high risks. Say bye to the impermanent loss!

## Vantra projects
* ✅ Vantra Token [(read more)](vantra-token.md)
* ⬜ Vantra Earn
* ⬜ Vantra Lottery
* ⬜ Pixel Vantra
* ⬜ Vantra Store
* ⬜ Vantra Games

## Projects roadmap
### April
* ✅ Launch website
* ⬜ Private presale of _**VANTRA TOKEN**_ 
* ⬜ Public presale of _**VANTRA TOKEN**_

[Read more about the presale](vantra-presale.md)

### May
* ⬜ End of _**VANTRA TOKEN**_ presale
* ⬜ Public sale of  _**VANTRA TOKEN**_
* ⬜ Launch _**Vantra Earn**_
* ⬜ Launch _**Pixel Vantra**_

### June
* ⬜ Launch _**Vantra Lottery**_

### Backlog (planning)
* ⬜ Launch _**Vantra Store**_
* ⬜ Launch _**Vantra Games**_

## General roadmap and backlog
* Social media
* Get alliance with popular investors
* Get audited
* List on CoinGecko or CoinMarketCap

----

Website: https://vantra.finance

Docs: https://docs.vantra.finance



