# Vantra Token
## Token information:

**Ticker**: VANTRA

**Contract Addres**: 
...

**Chain**: Binance Smart Chain (BEP-20)

**Total MAX supply**: auto-generated on demand.

**Decimals**: 18

**Not mineable.**

**Anti whale protection**: we limit the maximum amount of Vantra for each sale transaction (after pre-sale). For each transaction, a maximum of 5 BNB will be allowed to withdraw (this limit can be adjusted).

## Mechanics:

![Diagram](http://docs.vantra.finance/img/vantra_token_diagram.png)

[Open diagram](https://www.plantuml.com/plantuml/png/dLB1Qjmm4BtxA-OIi4zR2gNG79erFPMIGfAczAZ8uqPO7fcPqIw6_VXAjiowXLvoaHvFJ-zvddKdQbZZwAi5L9NheJvQTO4Qq5SagDiVlveAuDEp8MK36eR4liGPwhFJLsZls-Osdp_6QV8pEBACHh23YSCKE5iPx-UiwGCFQRkQ2P8cHuY2l0d0_3QELja3EZgXA1mEjOp91cwQmo4h0MJoJZFAmY_kv3eaWNCIeUuKnQxedj2CC2P2aGnP_A-mZKmfDJ0mziXi_9QBr9Ni5PUXHCDKeHU46d9VkSZlgK46yrUTtu8gyjn0jGNDu5SErDa15Uhdu_tJmx6vBP9o1HWbUfL2Bt-APZWx8kJFc_-Cqgo2KemekvUeU2Kly7Ev8mdbETEsz7BPIpv-k4dh-tTvpR2ahXDIs6SShOrZz4Onbm5VZHsce7kpGhqXZZhNPrWAtZxW4Rs7KbCY_tsUaezhHelkXEM9NZsAwxhUfIsE_Wy0)
