# How to add Binance Smart Chain on metamask

## Configuring the wallet
You might notice straight away that we’re still dealing with an Ethereum wallet. At best, this won’t work with Binance Smart Chain DApps. At worst, you could lose funds by sending them to addresses you can’t actually use.

Let’s change that. We want to access the Settings to point the wallet towards Binance Smart Chain nodes.

![Select settings from the dropdown menu](https://academy.binance.com/_next/image?url=https%3A%2F%2Fimage.binance.vision%2Feditor-uploads%2F47eadf5a6e684e199f8b178dd8c0dc89.png&w=3840&q=75)

On the Settings page, we want to locate the Networks menu.

![The Networks menu](https://academy.binance.com/_next/image?url=https%3A%2F%2Fimage.binance.vision%2Feditor-uploads%2F13dfcf5d5d8d4427b7e657bbbdb575c4.png&w=3840&q=75)

We want to click Add Network in the top-right corner to manually add the Binance Smart Chain one – it doesn’t come packaged with MetaMask. It’s important to note that there are two networks we can use here: the testnet or the mainnet. Below are the parameters to fill in for each.

## Network:
**Network Name**: Smart Chain

**New RPC URL**: https://bsc-dataseed.binance.org/

**ChainID**: 56

**Symbol**: BNB

**Block Explorer URL**: https://bscscan.com

​

Source :https://academy.binance.com/en/articles/connecting-metamask-to-binance-smart-chain
