# Vantra Token Presale 

BUY VANTRA TOKENS WITH BNB UNDER THE LAUCH PRICE.

Accepting: BNB.

Official presale link: https://vantra.finance/#/presale

## Private pre-sale
The private presale will be available only with referral link.

The token price for the private pre-sale will be locked. The users will get a discount.

## Public pre-sale
Vantra tokens will be sold in the presale from our website.

The token price for the public pre-sale will be locked. After the pre-sale, the token price will raise.

